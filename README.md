## Test your endpoints with this solution

This solution offers a solution to show headers, get and post data of the endpoints so that you can test any API's endpoints response with this solution.

## Installation and use

1. Clone the repository.
2. Go to the root directory.
3. Run node app.js or nodejs app.js.
4. Go to http://localhost:3000/