const express = require("express");
const app = express();
const path = require("path");
const bodyParser = require("body-parser");
const router = require("./core/router");
const port = process.env.PORT || 3000;

  app.use(express.static("public"));
  app.set("views", path.join(__dirname, "views"));
  app.set("view engine", "pug");

//Here we are configuring express to use body-parser as middle-ware.
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use("/", router);
  

const server = require("http").createServer(app);
const io = require("socket.io")(server);
socket = require("./core/socket")(io);
server.listen(port, () => console.log(`App listening on port ${port}!`));
