const express = require('express');
const router = express.Router();
const md5 = require('blueimp-md5');

// define the home page route
router.get('/', function (req, res, next) {
    res.render('index', { 
        title: 'Endpoint Tester', 
        url: req.protocol + '://' + req.get('host') + req.originalUrl + 'endpoint/'+ md5(Math.random())
    });
});


// define the about route
router.get('/endpoint/:page', function (req, res, next) {
    console.log(req.body);
    res.render('page', { title: 'Response', get: req.query, post: req.body, headers: req.headers})
});

// define the about route
router.post('/endpoint/:page', function (req, res, next) {
    res.render('page', { title: 'Response', request: req})
});

module.exports = router;